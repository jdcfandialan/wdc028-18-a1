function getInteger(number){
	if(isNaN(number) || number === true || number === false){
		console.log(`${number} is NaN!`);
	}

	else{
		if(number % 2 == 0){
			console.log(`${number} is even!`);
		}

		else console.log(`${number} is odd!`);
	}

	return null;
}

let month = "FEBRUARY";

switch(month.toLowerCase()){
	case "january":
		 console.log("Winter time");
		 break;

	case "february":
		 console.log("Love month");
		 break;

	case "march":
		 console.log("Graduation");
		 break;

	case "april":
		 console.log("Spring season");
		 break;

	case "may":
		 console.log("Labor day");
		 break;

	case "june":
		 console.log("Wedding month");
		 break;

	case "july":
		 console.log("Summertime");
		 break;

	case "august":
		 console.log("Almost back to school");
		 break;

	case "september":
		 console.log("Wake me up when September Ends");
		 break;

	case "october":
		 console.log("Ocotober Fest");
		 break;

	case "november":
		 console.log("No shave November");
		 break;

	case "december":
		 console.log("Happy holidays!");
		 break;

	default: console.error("invalid input!");
}